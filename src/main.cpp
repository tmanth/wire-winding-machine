#include <Arduino.h>
#include <IRremote.h>
#include <Wire.h>
#include <math.h>
// #include <LiquidCrystal_I2C.h>
#include <LCD_1602_RUS.h>

LCD_1602_RUS lcd(0x3F, 16, 2);
// LiquidCrystal_I2C lcd(0x3F, 16, 2);

uint8_t symbol_one[8] = {
  B11000,
  B11100,
  B11110,
  B11111,
  B11111,
  B11110,
  B11100,
  B11000,
};
IRrecv irrecv(2); // указываем вывод, к которому подключен приемник
String menu_array[] = {"KOL SLOEV","VITKOV V SLOE","DIAMETR PROWODA","START","RESTART"};
decode_results results;
byte menu_chapt = 0;
int layer_quantity = 0;
int coil_layer = 0;
byte cursor_position = 0;
byte cipher = 0;

bool float_point_check = false; // . => *
float diameter_wire = 0.0;
byte nums_after_point = 0;
byte quantity_nums_after_point = 0;
void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  irrecv.enableIRIn(); // запускаем прием
  lcd.init(); // Инициализация LCD
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("WIRE WINDING");
  lcd.setCursor(9,1);
  lcd.print("MACHINE");
  delay(3300);
  lcd.clear();
}

/*
  |  1  | - 0xFFA25D|
  |  2  | - 0xFF629D|
  |  3  | - 0xFFE21D|
  |  4  | - 0xFF22DD|
  |  5  | - 0xFF02FD|
  |  6  | - 0xFFC23D|
  |  7  | - 0xFFE01F|
  |  8  | - 0xFFA857|
  |  9  | - 0xFF906F|
  |  0  | - 0xFF986F|
  |  *  | - 0xFF6897|
  |  #  | - 0xFFB047|
  | OK  | - 0xFF38C7|
  | UP  | - 0xFF18E7|
  |DOWN | - 0xFF4AB5|
  |LEFT | - 0xFF10EF|
  |RIGHT| - 0xFF5AA5|
*/
    
int integer_numder_create(int ciphers, int integer_numder){
  lcd.print(ciphers,DEC);
  if (integer_numder == 0){
    integer_numder = ciphers;
  }else{
    integer_numder = (integer_numder * 10) + ciphers;
  }
  return integer_numder;
}
void menu(){
  //float parameters_winding[4] ={}
  lcd.createChar(6, symbol_one);
  lcd.setCursor(0, 0);
  lcd.write(6);
  lcd.setCursor(1, 0);
  lcd.print(menu_array[menu_chapt]);
  lcd.setCursor(1, 1);
  if (menu_chapt < (sizeof(menu_array)/sizeof(menu_array[0])-1)){
    lcd.print(menu_array[menu_chapt+1]);
  }else{
    if (menu_chapt >= (sizeof(menu_array)/sizeof(menu_array[0])-1) ){
      lcd.print("               ");
    }
  }

  if ( irrecv.decode( &results )) { // если данные пришли
    lcd.clear();
    if (results.value == 0xFF4AB5 && menu_chapt < (sizeof(menu_array)/sizeof(menu_array[0]) - 1) )//button (DOWN)
      ++ menu_chapt;
    if (results.value == 0xFF18E7 && menu_chapt > 0) //button (UP)
      -- menu_chapt;
    
    /*
      switch ( results.value ) {
      case 0xFFA25D: //button (1)
          lcd.print(menu_array[3]);
          break;
      case 0xFF629D: //button (2)
          lcd.print("2");
          break;
      case 0xFFE21D: //button (3)
          lcd.print("3");
          break;
      case 0xFF22DD: //button (4)
          lcd.print("4");
          break;
      case 0xFF02FD: //button (5)
          lcd.print("5");
          break;
      case 0xFFC23D: //button (6)
          lcd.print("6");
          break;
      case 0xFFE01F: //button (7)
          lcd.print("7");
          break;
      case 0xFFA857: //button (8)
          lcd.print("8");
          break;
      case 0xFF906F: //button (9)
          lcd.print("9");
          break;
      case 0xFF9867: //button (0)
          lcd.print("0");
          break;
      case 0xFF6897: //button (*)
          lcd.print("*");
          break;
      case 0xFFB04F: //button (#)
          lcd.print("#");
          break;
      case 0xFF38C7: //button (OK)
          lcd.print("OK");
          break;
      case 0xFF18E7: //button (UP)
          lcd.print("ВВЕРХ");
          break;
      case 0xFF4AB5: //button (DOWN)
          lcd.print("ВНИЗ");
          break;
      case 0xFF10EF: //button (LEFT)
          lcd.print("ВЛЕВО");
          break;
      case 0xFF5AA5: //button (RIGHT)
          lcd.print("ВПРАВО");
          break;
      default:
          Serial.println(results.value, HEX);
          break;
    }
    */    
    
    if (results.value == 0xFF38C7){ //button OK
      irrecv.resume(); // принимаем следующую команду
      switch (menu_chapt){
        case 0: // ввод колличества слоев
          layer_quantity = 0;
          lcd.clear();
          lcd.print("Sloev:");
          lcd.setCursor(0, 1);
          lcd.blink();
          while(1){
            if ( irrecv.decode( &results )) {
              switch (results.value){
                case 0xFFA25D: //button (1)
                  layer_quantity = integer_numder_create(1, layer_quantity);
                  ++ cursor_position;
                  break;
                case 0xFF629D: //button (2)
                  layer_quantity = integer_numder_create(2, layer_quantity);
                  ++ cursor_position;
                  break;
                case 0xFFE21D: //button (3)
                  layer_quantity = integer_numder_create(3, layer_quantity);
                  ++ cursor_position;
                  break;
                case 0xFF22DD: //button (4)
                  layer_quantity = integer_numder_create(4, layer_quantity);
                  ++ cursor_position;
                  break;
                case 0xFF02FD: //button (5)
                  layer_quantity = integer_numder_create(5, layer_quantity);
                  ++ cursor_position;
                  break;
                case 0xFFC23D: //button (6)
                  layer_quantity = integer_numder_create(6, layer_quantity);
                  ++ cursor_position;
                  break;
                case 0xFFE01F: //button (7)
                  layer_quantity = integer_numder_create(7, layer_quantity);
                  ++ cursor_position;
                  break;
                case 0xFFA857: //button (8)
                  layer_quantity = integer_numder_create(8, layer_quantity);
                  ++ cursor_position;
                  break;
                case 0xFF906F: //button (9)
                  layer_quantity = integer_numder_create(9, layer_quantity);
                  ++ cursor_position;
                  break;
                case 0xFF9867: //button (0)
                  lcd.print(0);
                  if (layer_quantity > 0){
                    layer_quantity = layer_quantity*10;
                  }
                  ++ cursor_position;
                  break;
                
                case 0xFF38C7: // button (OK)
                  Serial.println(layer_quantity);
                  cursor_position = 0;
                  lcd.clear();
                  break;
                case 0xFF10EF: //button (LEFT)
                  layer_quantity = (layer_quantity - (layer_quantity % 10))/10;
                  Serial.println(layer_quantity);
                  -- cursor_position;
                  lcd.setCursor(cursor_position, 1);
                  lcd.print(" ");
                  lcd.setCursor(cursor_position, 1);
                  break;
                default:
                  break;
              }
              
              if (results.value == 0xFF38C7){ // button (OK)
                irrecv.resume(); // принимаем следующую команду
                break;
              }
              irrecv.resume(); // принимаем следующую команду
            }
          }
          break;
        case 1:
          coil_layer = 0;
          lcd.clear();
          lcd.print("Sloev:");
          lcd.setCursor(0, 1);
          lcd.blink();
          while(1){
            if ( irrecv.decode( &results )) {
              switch (results.value){
                case 0xFFA25D: //button (1)
                  coil_layer = integer_numder_create(1, coil_layer);
                  ++ cursor_position;
                  break;
                case 0xFF629D: //button (2)
                  coil_layer = integer_numder_create(2, coil_layer);
                  ++ cursor_position;
                  break;
                case 0xFFE21D: //button (3)
                  coil_layer = integer_numder_create(3, coil_layer);
                  ++ cursor_position;
                  break;
                case 0xFF22DD: //button (4)
                  coil_layer = integer_numder_create(4, coil_layer);
                  ++ cursor_position;
                  break;
                case 0xFF02FD: //button (5)
                  coil_layer = integer_numder_create(5, coil_layer);
                  ++ cursor_position;
                  break;
                case 0xFFC23D: //button (6)
                  coil_layer = integer_numder_create(6, coil_layer);
                  ++ cursor_position;
                  break;
                case 0xFFE01F: //button (7)
                  coil_layer = integer_numder_create(7, coil_layer);
                  ++ cursor_position;
                  break;
                case 0xFFA857: //button (8)
                  coil_layer = integer_numder_create(8, coil_layer);
                  ++ cursor_position;
                  break;
                case 0xFF906F: //button (9)
                  coil_layer = integer_numder_create(9, coil_layer);
                  ++ cursor_position;
                  break;
                case 0xFF9867: //button (0)
                  lcd.print(0);
                  if (coil_layer > 0){
                    coil_layer = coil_layer*10;
                  }
                  ++ cursor_position;
                  break;
                
                case 0xFF38C7: // button (OK)
                  Serial.println(coil_layer);
                  cursor_position = 0;
                  lcd.clear();
                  break;
                case 0xFF10EF: //button (LEFT)
                  coil_layer = (coil_layer - (coil_layer % 10))/10;
                  Serial.println(coil_layer);
                  -- cursor_position;
                  lcd.setCursor(cursor_position, 1);
                  lcd.print(" ");
                  lcd.setCursor(cursor_position, 1);
                  break;
                default:
                  break;
              }
              
              if (results.value == 0xFF38C7){ // button (OK)
                irrecv.resume(); // принимаем следующую команду
                break;
              }
              irrecv.resume(); // принимаем следующую команду
            }
          }
          break;
        case 2: // ввод диаметра провода
          diameter_wire = 0;
          lcd.clear();
          lcd.print("WIRE DIAMETER:");
          lcd.setCursor(0, 1);
          lcd.blink();
          while(1){
            if (irrecv.decode( &results )){
              Serial.println(diameter_wire, DEC);
              switch (results.value){
                case 0xFF6897: //button (*)
                  if (diameter_wire == 0.0){
                    lcd.print("0.");
                    cursor_position = cursor_position + 2;
                  }else{
                    if (diameter_wire > 0.0){
                      lcd.print(".");
                      ++ cursor_position;
                    }
                  }
                  float_point_check = true;
                  break;
                
                case 0xFFA25D: //button (1)
                  cipher = 1;
                  lcd.print(cipher, DEC);
                  ++ cursor_position;
                    if (!float_point_check)
                    {
                      if (diameter_wire == 0.0){
                        diameter_wire = cipher;
                      }else{
                        if (diameter_wire > 0.0){
                          diameter_wire = diameter_wire*10 + cipher;
                        } 
                      }
                    }
                    if (float_point_check)
                    {
                      ++ nums_after_point;
                      diameter_wire = diameter_wire + cipher/pow(10,nums_after_point);
                    }
                  break;
                
                case 0xFF629D: //button (2)
                  cipher = 2;
                  lcd.print(cipher, DEC);
                  ++ cursor_position;
                    if (!float_point_check)
                    {
                      if (diameter_wire == 0.0){
                        diameter_wire = cipher;
                      }else{
                        if (diameter_wire > 0.0){
                          diameter_wire = diameter_wire*10 + cipher;
                        } 
                      }
                    }
                    if (float_point_check)
                    {
                      ++ nums_after_point;
                      diameter_wire = diameter_wire + cipher/pow(10,nums_after_point);
                    }
                  break;
                case 0xFFE21D: //button (3)
                  cipher = 3;
                  lcd.print(cipher, DEC);
                  ++ cursor_position;
                    if (!float_point_check)
                    {
                      if (diameter_wire == 0.0){
                        diameter_wire = cipher;
                      }else{
                        if (diameter_wire > 0.0){
                          diameter_wire = diameter_wire*10 + cipher;
                        } 
                      }
                    }
                    if (float_point_check)
                    {
                      ++ nums_after_point;
                      diameter_wire = diameter_wire + cipher/pow(10,nums_after_point);
                    }
                  break;
                case 0xFF22DD: //button (4)
                  cipher = 4;
                  lcd.print(cipher, DEC);
                  ++ cursor_position;
                    if (!float_point_check)
                    {
                      if (diameter_wire == 0.0){
                        diameter_wire = cipher;
                      }else{
                        if (diameter_wire > 0.0){
                          diameter_wire = diameter_wire*10 + cipher;
                        } 
                      }
                    }
                    if (float_point_check)
                    {
                      ++ nums_after_point;
                      diameter_wire = diameter_wire + cipher/pow(10,nums_after_point);
                    }
                  break;
                case 0xFF02FD: //button (5)
                  cipher = 5;
                  lcd.print(cipher, DEC);
                  ++ cursor_position;
                    if (!float_point_check)
                    {
                      if (diameter_wire == 0.0){
                        diameter_wire = cipher;
                      }else{
                        if (diameter_wire > 0.0){
                          diameter_wire = diameter_wire*10 + cipher;
                        } 
                      }
                    }
                    if (float_point_check)
                    {
                      ++ nums_after_point;
                      diameter_wire = diameter_wire + cipher/pow(10,nums_after_point);
                    }
                  break;
                case 0xFFC23D: //button (6)
                  cipher = 6;
                  lcd.print(cipher, DEC);
                  ++ cursor_position;
                    if (!float_point_check)
                    {
                      if (diameter_wire == 0.0){
                        diameter_wire = cipher;
                      }else{
                        if (diameter_wire > 0.0){
                          diameter_wire = diameter_wire*10 + cipher;
                        } 
                      }
                    }
                    if (float_point_check)
                    {
                      ++ nums_after_point;
                      diameter_wire = diameter_wire + cipher/pow(10,nums_after_point);
                    }
                  break;
                case 0xFFE01F: //button (7)
                  cipher = 7;
                  lcd.print(cipher, DEC);
                  ++ cursor_position;
                    if (!float_point_check)
                    {
                      if (diameter_wire == 0.0){
                        diameter_wire = cipher;
                      }else{
                        if (diameter_wire > 0.0){
                          diameter_wire = diameter_wire*10 + cipher;
                        } 
                      }
                    }
                    if (float_point_check)
                    {
                      ++ nums_after_point;
                      diameter_wire = diameter_wire + cipher/pow(10,nums_after_point);
                    }
                  break;
                case 0xFFA857: //button (8)
                  cipher = 8;
                  lcd.print(cipher, DEC);
                  ++ cursor_position;
                    if (!float_point_check)
                    {
                      if (diameter_wire == 0.0){
                        diameter_wire = cipher;
                      }else{
                        if (diameter_wire > 0.0){
                          diameter_wire = diameter_wire*10 + cipher;
                        } 
                      }
                    }
                    if (float_point_check)
                    {
                      ++ nums_after_point;
                      diameter_wire = diameter_wire + cipher/pow(10,nums_after_point);
                    }
                  break;
                case 0xFF906F: //button (9)
                  cipher = 9;
                  lcd.print(cipher, DEC);
                  ++ cursor_position;
                    if (!float_point_check)
                    {
                      if (diameter_wire == 0.0){
                        diameter_wire = cipher;
                      }else{
                        if (diameter_wire > 0.0){
                          diameter_wire = diameter_wire*10 + cipher;
                        } 
                      }
                    }
                    if (float_point_check)
                    {
                      ++ nums_after_point;
                      diameter_wire = diameter_wire + cipher/pow(10,nums_after_point);
                    }
                  break;
                case 0xFF9867: //button (0)
                  cipher = 0;
                  lcd.print(cipher, DEC);
                  ++ cursor_position;
                    if (!float_point_check)
                    {
                      if (diameter_wire == 0.0){
                        diameter_wire = cipher;
                      }else{
                        if (diameter_wire > 0.0){
                          diameter_wire = diameter_wire*10 + cipher;
                        } 
                      }
                    }
                    if (float_point_check)
                    {
                      ++ nums_after_point;
                      diameter_wire = diameter_wire + cipher/pow(10,nums_after_point);
                    }
                  break;
                case 0xFF38C7: // button (OK)
                  Serial.println(diameter_wire);
                  cursor_position = 0;
                  lcd.clear();
                  break;
                case 0xFF10EF: //button (LEFT)
                  diameter_wire = (diameter_wire - (diameter_wire % 10))/10;
                  Serial.println(diameter_wire);
                  -- cursor_position;
                  lcd.setCursor(cursor_position, 1);
                  lcd.print(" ");
                  lcd.setCursor(cursor_position, 1);
                  break;
                default:
                  break;
              }
               if (results.value == 0xFF38C7){ // button (OK)
                irrecv.resume(); // принимаем следующую команду
                break;
              }
              irrecv.resume();
            }
          }
          break;
        
        case 3: // старт намотки
          lcd.clear();
          lcd.print(layer_quantity, DEC);
          lcd.setCursor(8, 0);
          lcd.print(coil_layer, DEC);
          lcd.setCursor(0, 1);
          lcd.print(diameter_wire, DEC);
          delay(6000);
          lcd.clear();
          break;
        default:
          break;
      }
    lcd.noBlink();
    }
    irrecv.resume(); // принимаем следующую команду
  }
}
void loop() {
  // menu begin
  menu();
  //MENU end
}

// LiquidCrystal_I2C lcd(0x20, 16,2);
// bool check_press_button_up = false;
// bool check_press_button_down = false;
// bool encoder_status_pin_a = false;
// bool encoder_status_pin_b = false;
// int step_motor = 0;
// uint32_t button_up_push_time = 0;
// uint32_t button_down_push_time = 0;
// uint32_t rotor_time = 0;
// float step_motor_speed = 10;
// bool led_state = false;
// void step_motor_run(byte pin_step_motor_1, byte pin_step_motor_2, byte pin_step_motor_3, byte pin_step_motor_4, int speed=100){
//   digitalWrite(13, HIGH);
//   speed = 21/speed;
//   digitalWrite(pin_step_motor_1, HIGH);
//   delay(speed);  
//   digitalWrite(pin_step_motor_2,HIGH);
//   delay(speed);
//   digitalWrite(pin_step_motor_1,LOW);
//   delay(speed);
//   digitalWrite(pin_step_motor_3,HIGH);
//   delay(speed);
//   digitalWrite(pin_step_motor_2,LOW);
//   delay(speed);
//   digitalWrite(pin_step_motor_4,HIGH);
//   delay(speed);
//   digitalWrite(pin_step_motor_3,LOW);
//   delay(speed);
//   digitalWrite(pin_step_motor_1, HIGH);
//   delay(speed);
//   digitalWrite(pin_step_motor_4,LOW);
//   digitalWrite(13, LOW);
// }

// void setup(){ 
//   pinMode(7, INPUT); // UP step motor speed button
//   pinMode(6, INPUT); // DOWN step motor speed button

//   pinMode(2, OUTPUT); // step motor pin
//   pinMode(3, OUTPUT); // step motor pin
//   pinMode(4, OUTPUT); // step motor pin
//   pinMode(5, OUTPUT); // step motor pin
  
//   pinMode(13, OUTPUT);
  
//   lcd.init();                      // Инициализация дисплея  
//   lcd.backlight();                 // Подключение подсветки
//   lcd.setCursor(0,0);              // Установка курсора в начало первой строки
//   lcd.print("Hello");
//   lcd.clear();
// }
// void loop() {
//   uint32_t programm_life_time = millis();
  
//   lcd.setCursor(0, 0);
//   lcd.clear();
//   lcd.print(step_motor);
//   lcd.setCursor(0, 1);
//   lcd.print(step_motor_speed/10);
//   //step_motor_run(2, 3, 4, 5, step_motor_speed/10);
  
//   // encoder_status_pin_a, encoder_status_pin_b
//   if ((digitalRead(6) == LOW && digitalRead(7) == LOW) ){
//     if(encoder_status_pin_a == true && encoder_status_pin_b == false){
//       step_motor = step_motor + 1;
//     }
//     if(encoder_status_pin_a == true && encoder_status_pin_b == true){
//       step_motor = step_motor - 1;
//     }
//   }

//   if ((digitalRead(6) == HIGH && digitalRead(7) == LOW) ){
//     if(encoder_status_pin_a == false && encoder_status_pin_b == true){
//       step_motor = step_motor + 1;
//     }
//     if(encoder_status_pin_a == false && encoder_status_pin_b == false){
//       step_motor = step_motor - 1;
//     }
//   }

//   if ((digitalRead(6) == LOW && digitalRead(7) == HIGH) ){
//     if(encoder_status_pin_a == true && encoder_status_pin_b == true){
//       step_motor = step_motor + 1;
//     }
//     if(encoder_status_pin_a == true && encoder_status_pin_b == false){
//       step_motor = step_motor - 1;
//     }
//   }

//   if ((digitalRead(6) == HIGH && digitalRead(7) == HIGH) ){
//     if(encoder_status_pin_a == false && encoder_status_pin_b == false){
//       step_motor = step_motor + 1;
//     }
//     if(encoder_status_pin_a == false && encoder_status_pin_b == true){
//       step_motor = step_motor - 1;
//     }
//   }

//   switch (digitalRead(7)){
//     case HIGH:
//       encoder_status_pin_a = true;
//       break;
//     case LOW:
//       encoder_status_pin_a = false;
//       break;
//     default:
//       break;
//   }
//   switch (digitalRead(6)){
//     case HIGH:
//       encoder_status_pin_b = true;
//       break;
//     case LOW:
//       encoder_status_pin_b = false;
//       break;
//     default:
//       break;
//   }
  

//   // if (digitalRead(7) == HIGH && check_press_button_up == false){
//   //   check_press_button_up = true;
//   //   step_motor_speed = step_motor_speed + 1;
//   // }else{
//   //   check_press_button_up = false;
//   // }
//   // if (digitalRead(6) == HIGH && check_press_button_down == false){
//   //   check_press_button_down = true;
//   //   if (step_motor_speed > 10){
//   //     step_motor_speed = step_motor_speed - 1;
//   //   }
//   // }else{
//   //   check_press_button_down = false;
//   // }
// } 